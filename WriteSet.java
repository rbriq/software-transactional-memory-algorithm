package org.deuce.transaction.nbstm;

import java.util.Iterator;

import org.deuce.transaction.nbstm.field.WriteFieldAccess;
import org.deuce.trove.THashSet;

public class WriteSet implements Iterable<WriteFieldAccess> {

	final private THashSet<WriteFieldAccess> writeSet = new THashSet<WriteFieldAccess>();

	public void clear() {
		writeSet.clear();
	}

	public void put(WriteFieldAccess write) {
		// Add to write set
		if (!writeSet.add(write))
			writeSet.replace(write);
	}

	public void writeToMainMemory() {
		// Write values to main memory
		for (WriteFieldAccess write : writeSet) {
			System.out.println("write value: " + write.getValue());
			write.writeField();
		}
	}

	public Iterator<WriteFieldAccess> iterator() {
		return writeSet.iterator();
	}

	public WriteFieldAccess get(Object ref) {
		return writeSet.get(ref);
	}

	public int size() {
		return writeSet.size();
	}

}
