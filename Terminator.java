package org.deuce.transaction.nbstm;

public class Terminator implements Runnable {

	public static int sleepTime;

	static {
		sleepTime = 0;
		String sleepTimeStr = System.getProperty("terminatorSleepTime");
		if (sleepTimeStr != null) {
			sleepTime = Integer.parseInt(sleepTimeStr);
		}
	}

	private void cleanUp() {
		TransactionDescriptor currTxn;
		TransactionDescriptor prev;
		for (currTxn = Context.getLastCompleted(), prev = currTxn; currTxn != null; prev = currTxn, currTxn = currTxn
				.getNext()) {
			System.out.println("currTxn in terminator: " + currTxn );
			boolean isWritten = currTxn.terminateBarrier();
			if (!isWritten) {
				 currTxn.getWriteSet().writeToMainMemory();
				 System.out.println("write set written to memory");
			}
		}
		System.out.println("clearup prev: " + prev);
		Context.setLastCompleted(prev);
	}

	@Override
	public void run() {
		while (true) {
			cleanUp();
		}
	}

}
