package org.deuce.transaction.nbstm.field;


import org.deuce.transaction.nbstm.field.Field.Type;

public class WriteFieldAccess extends ReadFieldAccess {
	
	final private Type type;
	private Object value;

	public WriteFieldAccess(Object reference, long field, Type type, Object value) {
		super(reference, field);
		this.type = type;
		this.value = value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return value;
	}


	public void writeField() {
		Field.putValue(reference, field, value, type);
	}

}
