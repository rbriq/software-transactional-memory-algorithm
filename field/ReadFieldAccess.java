package org.deuce.transaction.nbstm.field;


public class ReadFieldAccess {
	protected Object reference;
	protected long field;
	private int hash;

	public ReadFieldAccess(){}
	
	public ReadFieldAccess( Object reference, long field){
		init(reference, field);
	}
	
	public void init( Object reference, long field){
		this.reference = reference;
		this.field = field;
		this.hash = (System.identityHashCode( reference) + (int)field);// & LockTable.MASK;
	}

	@Override
	public boolean equals( Object obj){
		ReadFieldAccess other = (ReadFieldAccess)obj;
		return reference == other.reference && field == other.field;
	}

	@Override
	final public int hashCode(){
		return hash;
	}

	public void clear(){
		reference = null;
	}
	
	public Object getReference()
	{
		return reference;
	}
	
	public long getField()
	{
		return field;
	}
}
