package org.deuce.transaction.nbstm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.deuce.transaction.nbstm.field.ReadFieldAccess;

public class ReadSet {
	//private static final int DEFAULT_CAPACITY = 1024;

	private List <ReadFieldAccess> entries;
	
	public ReadSet() {
		entries = new ArrayList<ReadFieldAccess>() ;
	}
	
	public void add(Object reference, long field) {
		ReadFieldAccess r = new ReadFieldAccess(reference, field);
		entries.add(r);	
	}
	
	public void clear() {
		entries.clear();
	}
	
	public boolean contains(Object reference) {
		return entries.contains(reference);
	}

	public Iterator<ReadFieldAccess> iterator() {
		// TODO Auto-generated method stub
		return entries.iterator();
	}
}
