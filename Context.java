package org.deuce.transaction.nbstm;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.deuce.transaction.nbstm.field.ReadFieldAccess;
import org.deuce.transaction.nbstm.field.WriteFieldAccess;
import org.deuce.transaction.nbstm.field.Field.Type;

final public class Context implements org.deuce.transaction.Context {

	final private WriteSet lookupTable = new WriteSet();
	BloomFilter<ReadFieldAccess> readSetBF = new BloomFilter<ReadFieldAccess>(
			1000, 100);
	private Object readValue;

	// points to the current transaction of the thread
	private TransactionDescriptor currentTxn;

	// points to the latest committed transaction
	private static AtomicReference<TransactionDescriptor> lastCommitted = new AtomicReference<TransactionDescriptor>();

	// points to the first transaction, whose values have not been
	// written to the main memory, i.e., it's the first to be written to the
	// main memory.
	private static AtomicReference<TransactionDescriptor> lastCompleted = new AtomicReference<TransactionDescriptor>();

	private static final TransactionDescriptor initTxnDsc; // a dummy list head

	private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	// private final Lock readLock = readWriteLock.readLock();
	private final Lock writeLock = readWriteLock.writeLock();

	static {
		initTxnDsc = new TransactionDescriptor();
		lastCommitted.set(initTxnDsc);
		lastCompleted.set(initTxnDsc);
		System.out.println("Hi " + initTxnDsc);
		new Thread(new Terminator()).start();
	}

	public Context() {

	}

	@Override
	public void init(int atomicBlockId, String metainf) {

		currentTxn = new TransactionDescriptor();
		this.lookupTable.clear();
		this.readSetBF.clear();

		TransactionDescriptor tempTxn;
		TransactionDescriptor prev;
		// TransactionDescriptor lastComp = lastCompleted.get();
		for (tempTxn = lastCompleted.get(), prev = tempTxn; tempTxn != null; prev = tempTxn, tempTxn = tempTxn
				.getNext()) {
			// System.out.println("init txn: " + tempTxn );
			if (tempTxn.getWriteSet() != null) {
				updateLookupTable(tempTxn.getWriteSet());
			}
		}
		setBarrier(prev);
	}

	private void setBarrier(TransactionDescriptor txnDescriptor) {
		TransactionDescriptor prev = null;
		while (txnDescriptor.incrementBarrier() == false) {
			prev = txnDescriptor;
			txnDescriptor = txnDescriptor.getNext();
			if (txnDescriptor == null) {
				// System.out.println("incrementing last barrier");
				prev.incrementLastBarrier();
				currentTxn.setStartPoint(prev);
				currentTxn.setLastValidated(prev);
				return;
			}
			if (txnDescriptor != null)
				updateLookupTable(txnDescriptor.getWriteSet());
		}
		currentTxn.setStartPoint(txnDescriptor);
		currentTxn.setLastValidated(txnDescriptor);
	}

	private void updateLookupTable(WriteSet ws) {
		for (WriteFieldAccess wf : ws) {
			lookupTable.put(wf);
			System.out.println("lookup table values:" + wf.getValue());
		}
	}

	// used for some read validation that is irrelevant to our algorithm
	@Override
	public void beforeReadAccess(Object obj, long field) {
	}

	// unneeded in our algorithm in the mean time
	@Override
	public void onIrrevocableAccess() {

	}

	private boolean onReadAccess(Object obj, long field, Type type) {
		// first check this tx own write-set
		ReadFieldAccess CurrRead = new ReadFieldAccess(obj, field);
		WriteFieldAccess w = currentTxn.getWriteSet().get(CurrRead);
		if (w == null) {
			// then check lookup-table
			w = this.lookupTable.get(CurrRead);
		}
		// Save object to readset bloom filter
		ReadFieldAccess r = new ReadFieldAccess(obj, field);
		readSetBF.add(r);
		readValue = (w != null) ? w.getValue() : null;
		if (readValue != null)
			System.out.println("read value :" + readValue);
		return (readValue != null);
	}

	@Override
	public Object onReadAccess(Object obj, Object value, long field) {
		System.out.println("read value :" + readValue);
		return (onReadAccess(obj, field, Type.OBJECT) ? readValue : value);
	}

	@Override
	public boolean onReadAccess(Object obj, boolean value, long field) {
		return (onReadAccess(obj, field, Type.BOOLEAN) ? (Boolean) readValue
				: value);
	}

	@Override
	public byte onReadAccess(Object obj, byte value, long field) {
		return (onReadAccess(obj, field, Type.BYTE) ? ((Number) readValue)
				.byteValue() : value);
	}

	@Override
	public char onReadAccess(Object obj, char value, long field) {
		return (onReadAccess(obj, field, Type.CHAR) ? (Character) readValue
				: value);
	}

	@Override
	public short onReadAccess(Object obj, short value, long field) {
		return (onReadAccess(obj, field, Type.SHORT) ? ((Number) readValue)
				.shortValue() : value);
	}

	@Override
	public int onReadAccess(Object obj, int value, long field) {
		// System.out.println("read value :" + readValue);
		return (onReadAccess(obj, field, Type.INT) ? ((Number) readValue)
				.intValue() : value);
	}

	@Override
	public long onReadAccess(Object obj, long value, long field) {
		return (onReadAccess(obj, field, Type.LONG) ? ((Number) readValue)
				.longValue() : value);
	}

	@Override
	public float onReadAccess(Object obj, float value, long field) {
		return (onReadAccess(obj, field, Type.FLOAT) ? ((Number) readValue)
				.floatValue() : value);
	}

	@Override
	public double onReadAccess(Object obj, double value, long field) {
		return (onReadAccess(obj, field, Type.DOUBLE) ? ((Number) readValue)
				.doubleValue() : value);
	}

	private void onWriteAccess(Object obj, long field, Object value, Type type) {
		// modify the value in this tx write-set.
		WriteFieldAccess wFieldAccess = new WriteFieldAccess(obj, field, type,
				value);
		// wFieldAccess.writeField();
		// System.out.println("value added to curr ws: " + value);
		currentTxn.addWriteField(wFieldAccess);
	}

	@Override
	public void onWriteAccess(Object obj, Object value, long field) {
		// System.out.println("writeAccess");
		onWriteAccess(obj, field, value, Type.OBJECT);

	}

	@Override
	public void onWriteAccess(Object obj, boolean value, long field) {
		// System.out.println("writeAccess");
		onWriteAccess(obj, field, (Object) value, Type.BOOLEAN);

	}

	@Override
	public void onWriteAccess(Object obj, byte value, long field) {
		onWriteAccess(obj, field, (Object) value, Type.BYTE);

	}

	@Override
	public void onWriteAccess(Object obj, char value, long field) {
		onWriteAccess(obj, field, (Object) value, Type.CHAR);

	}

	@Override
	public void onWriteAccess(Object obj, short value, long field) {
		onWriteAccess(obj, field, (Object) value, Type.SHORT);

	}

	@Override
	public void onWriteAccess(Object obj, int value, long field) {
		onWriteAccess(obj, field, (Object) value, Type.INT);

	}

	@Override
	public void onWriteAccess(Object obj, long value, long field) {
		onWriteAccess(obj, field, (Object) value, Type.LONG);

	}

	@Override
	public void onWriteAccess(Object obj, float value, long field) {
		onWriteAccess(obj, field, (Object) value, Type.FLOAT);

	}

	@Override
	public void onWriteAccess(Object obj, double value, long field) {
		onWriteAccess(obj, field, (Object) value, Type.DOUBLE);

	}

	@Override
	public void rollback() {
		// System.out.println("rollback");
		currentTxn.clearTxnDescriptor();

	}

	public static TransactionDescriptor getLastCommitted() {
		return lastCommitted.get();
	}

	public static TransactionDescriptor getLastCompleted() {
		return lastCompleted.get();
	}

	public static void setLastCompleted(TransactionDescriptor lastComp) {
		lastCompleted.set(lastComp);
	}

	@Override
	public boolean commit() {
		while (true) {
			if (intersect() == false) {
				TransactionDescriptor tempStart = currentTxn.getStartPoint();
				if (appendTxn() == true) {
					tempStart.decrementBarrier();
					return true;
				}
				currentTxn.getLastValidated().incrementBarrier();
				currentTxn.setStartPoint(currentTxn.getLastValidated());
				tempStart.decrementBarrier();
			} else {
				return false;
			}
		}
	}

	private boolean appendTxn() {
		writeLock.lock();
		try {
			if (lastCommitted.compareAndSet(currentTxn.getLastValidated(),
					lastCommitted.get()) == true) {
				// System.out.println("appending succeeded");
				lastCommitted.get().setNext(currentTxn);
				lastCommitted.set(currentTxn);
				return true;
			} else {
				// System.out.println("appending failed");
				return false;
			}
		} finally {
			writeLock.unlock();
		}
	}

	private boolean intersect() {
		TransactionDescriptor start = currentTxn.getStartPoint();
		TransactionDescriptor tempTx = start.getNext();
		while (tempTx != null) {
			Iterator<WriteFieldAccess> currIterWs = tempTx.getWriteSet()
					.iterator();
			WriteFieldAccess currW;

			currentTxn.setLastValidated(tempTx);
			while (currIterWs.hasNext()) {
				currW = currIterWs.next();
				ReadFieldAccess r = new ReadFieldAccess(currW.getReference(),
						currW.getField());
				if (readSetBF.contains(r)) {
					System.out.println("intersection not empty");
					return true; // intersection is not empty
				}
			}
			tempTx = tempTx.getNext();
		}
		return false;
	}
}