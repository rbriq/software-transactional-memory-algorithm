package org.deuce.transaction.nbstm;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

import org.deuce.transaction.nbstm.field.WriteFieldAccess;

public class TransactionDescriptor {
	
	final private WriteSet writeSet;
	
	private AtomicInteger barrier;
	private TransactionDescriptor startTxDescriptor = null;
	private volatile TransactionDescriptor next = null;
	private TransactionDescriptor lastValidatedAgainst = null;
	
	public TransactionDescriptor() {
		writeSet = new WriteSet();
		barrier = new AtomicInteger(0);
	}

	public Iterator<WriteFieldAccess> getWriteSetIter() {
		return writeSet.iterator();
	}

	public void setStartPoint(TransactionDescriptor txDescriptor) {
		this.startTxDescriptor = txDescriptor;
	}

	public TransactionDescriptor getStartPoint() {
		return this.startTxDescriptor;
	}

	public void clearWriteSet() {
		writeSet.clear();
	}

	public WriteSet getWriteSet() {
		return writeSet;
	}

	public void addWriteField(WriteFieldAccess wFieldAccess) {
		writeSet.put(wFieldAccess);
	}

	public TransactionDescriptor getNext() {
		return next;
	}

	public void setNext(TransactionDescriptor currentTxn) {
		this.next = currentTxn;
	}

	
	public void decrementBarrier() {
		this.barrier.decrementAndGet();
		synchronized (this) {
			if (this.barrier.get() == 0) {
				this.notifyAll();
				//System.out.println("notifying barrier value is now 0 " + this);
			}
		}
	}

	public boolean terminateBarrier() {
		// if the value of the barrier is -1, return true
		// indicating memory contents have already been 
		// marked for writing.
		//System.out.println("terminate barrier:" + barrier.get());
		if (barrier.get() == -1) {
			System.out.println("terminate barrier: value is -1"); 
			return true;
		}
		
		while(true) {
			int val = barrier.get();
			synchronized(this) {
				if (val > 0) {
					System.out.println("terminate barrier waiting: " + barrier.get() + " " + this);
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			if (barrier.compareAndSet(0, -1)) return false;	
		}
	}
	
	public Integer getBarrier() {
		return this.barrier.get();
	}
	
	public boolean incrementBarrier() {
		while (true) {
			int val = barrier.get();
			if (val < 0)
				return false;
			if (barrier.compareAndSet(val, val + 1))
				return true;
		}
	}
	
	public void incrementLastBarrier() {
		while ((barrier.compareAndSet(-1, 1) == false)) {
			if (incrementBarrier() == false) {
				continue;
			}
			else {
				break;
			}
		}
	}
	
	public void setLastValidated(TransactionDescriptor txDescriptor) {
		this.lastValidatedAgainst = txDescriptor;
	}

	public TransactionDescriptor getLastValidated() {
		return this.lastValidatedAgainst;
	}
	
	public void clearTxnDescriptor() {
		this.getStartPoint().decrementBarrier();
		this.writeSet.clear();
		this.barrier.set(0);
		
	}
}
